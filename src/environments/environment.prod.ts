// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: true,
  jsonapi: 'http://prod-479b4ade-479b4f03-lhl8inzv.app.devpanel.com/api',
  baseUrl: 'http://prod-479b4ade-479b4f03-lhl8inzv.app.devpanel.com',
  // jsonapi: 'https://live-contentacms.pantheonsite.io/api',
  // baseUrl: 'https://live-contentacms.pantheonsite.io',

};
